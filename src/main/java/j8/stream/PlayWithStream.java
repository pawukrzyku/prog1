package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;


public class PlayWithStream {

    public static void main(String[] args) {
        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

        ex1(employees);
        ex2(employees);
        ex3(employees);
        ex4(employees);
        ex5(employees);
        ex6(employees);
        ex7(employees);
        ex8(employees);
        ex9(employees);
        ex10(employees);
        ex11(employees);
        ex12(employees);
        ex13(employees);
        ex14(employees);
        ex14a(employees);
        ex14c(employees);
        ex15(employees);
        ex16(employees);
        ex17(employees);
        ex18(employees);
        ex19(employees);
        ex20(employees);
    }
//ZADANIE 1
//Wypisz osoby których wynagorodzenie jest w przedziale 2500-3199

    private static void ex1(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 1");
        employees.stream()
                .filter(e -> e.getSalary() > 2500 && e.getSalary() < 3199)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

//ZADANIE 2
//Wypisz osoby o wieku parzystym


//        List<Employee> evenAge = employees.stream()
//                .filter(e -> e.getAge() % 2 == 0)
//                .collect(Collectors.toList());
//        System.out.println(evenAge);

    private static void ex2(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 2");
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

//ZADANIE 3
//Wypisz osoby których nazwisko kończy się na ska i pracują na cały etat
//
//        List<Employee> endsWithSkaAndFullPartJob = employees.stream()
//                .filter(e -> e.getLastName().endsWith("ska") && e.getType().equals(ContractType.F))
//                .collect(Collectors.toList());
//        System.out.println(endsWithSkaAndFullPartJob);
//
//        //ZADANIE 3
//        private static void ex3 (List < Employee > employees) {
//            employees.stream()
//                    .filter(e -> e.getLastName().endsWith("ska") && e.getType().equals(ContractType.F))
//                    .forEach(System.out.println();

    private static void ex3(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 3");

        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska") && e.getType().equals(ContractType.F))
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

////        ZADANIE 4
//          Wypisz imiona wszystkich pracowników

////        List<Employee> onlyNamesOfAllEmployee = employees.stream()
////                .map(Employee::getFirstName)
////                .forEach(System.out::println);
//

    private static void ex4(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 4");

        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }

//    ZADANIE 5
//    Wypisz 3 ostatnie litery z naziwska, jeśli kończa się na ski/ska maja być z dużych liter

    private static void ex5(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 5");

        employees.stream()
                .map(Employee::getLastName)
                .map(e -> e.substring(e.length() - 3))
                .map(e -> (e.endsWith("ski") || e.endsWith("ska")) ? e.toUpperCase() : e)
                .forEach(System.out::println);
    }

    //ZADANIE 6
    //Sprawdź czy w liscie instnieje osoba o nazwisku "Kowalski", jedną instrukcją

    private static void ex6(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 6");

        employees.stream()
                .map(Employee::getProfession)
                .map(e -> e.toUpperCase())
                .forEach(System.out::println);
    }

    //ZADANIE 7
    //Sprawdź czy w liscie instnieje osoba o nazwisku "Kowalski", jedną instrukcją
    private static void ex7(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 7");

        boolean isKowalska = employees.stream()
                .anyMatch(e -> e.getLastName().equals("Kowalski"));
        System.out.println(isKowalska);
    }

    //ZADANIE 8
//    Zasymuluj podwyżki pracowników o 12%, tworząc wynikową mapę której kluczem będzie nazwisko a wartościa wynagrodzenie po podwyżce
    private static void ex8(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 8");

        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                Employee::getLastName,
                                Collectors.mapping(
                                        e -> e.getSalary() * 1.12, toList()
                                )
                        )
                );
        System.out.println(collect);
    }

    // Zadanie 9
//    Wyfiltruj pracowników tak, aby pozostawić tych których drugi znak imienia to 'a' a czwarty nazwiska to 'b'
    private static void ex9(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 9");

        employees.stream()
                .filter(e -> e.getFirstName().charAt(2) == 'a')
                .filter(e -> e.getLastName().charAt(2) == 'b')
                .forEach(System.out::println);
    }

    //Zadanie 10
//    Posortuj listę osób w następujący sposób - wpierw nazwisko alfabetycznie rosnąco, następnie imię.
    // tak bylo przed interfejsem:
//    private static void ex10(List<Employee> employees)
//        employees.stream()
//                .sorted(
//                        (person1, person2) -> {
//                            int lastNameComparator = person1.getLastName()
//                                    .compareTo(person2.getLastName());
//
//                            if (lastNameComparator != 0){
//                                return lastNameComparator;
//                            }
//                            return person1.getLastName()
//                                    .compareTo(person2.getFirstName());
//                        }
//                ).forEach(System.out::println);

    // TAK PO INTERFEJSCIE:

    private static void ex10(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 10");

        employees.stream()
                .sorted(
                        Comparator.comparing(Employee::getLastName)
                                .thenComparing(Employee::getFirstName)
                )
                .forEach(System.out::println);
    }

    //Zadanie 11
//Wypisz wszystkie nazwiska w formacie {lastName,lastName}
    private static void ex11(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 11");

        String lastNameOnly = employees.stream()
                .map(Employee::getLastName)
                .collect(Collectors.joining(","));
        System.out.println(lastNameOnly);
    }

//Zadanie 12
//Wypisz ilość osób zarabiających wiecęj niż 5000, w formacie true->20, false->30 (patritinioningBy() + couting())

    private static void ex12(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 12");

        Map<Boolean, Long> collect = employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,  // mozna sortowac po profesji, wieku itp
                                Collectors.counting()
                        )
                );
        System.out.println(collect);
    }

//Zadanie 13
//Wypisz średnią zarobków osób pracujących na pełny lub pół etatu

    private static void ex13(List<Employee> employees) {        //ctrl alt V tworzy zmienne i przypisuje do wartości//
        System.out.println(" ");
        System.out.println("ZADANIE 13");

        Map<String, Double> collect = employees.stream()
                .collect(groupingBy(
                        e -> e.getType().name(),
                        averagingDouble(
                                Employee::getSalary

                        )
                        )
                );
        System.out.println(collect);        // mapa: jezli klucze unikalne to collectors to map, jezeli nie to groupingBy
    }

//Zadanie 14
//Stwórz mapę gdzie kluczem będzie Imię a wartościa List:

    private static void ex14(List<Employee> employees) {        //ctrl alt V tworzy zmienne i przypisuje do wartości//
        System.out.println(" ");
        System.out.println("ZADANIE 14");

        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                Employee::getFirstName
                        )
                );
        System.out.println(collect);
    }


//Zadanie 14a
// Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia
//
//    private static void ex14a(List<Employee> employees) {        //ctrl alt V tworzy zmienne i przypisuje do wartości//
//        Map<String, Long> collect = employees.stream()
//                .collect(
//                        groupingBy(                         //jak by bylo .map to by byl exception
//                                e -> e.getFirstName(),
//                                Collectors.counting()       // j8 readme collecting and then,
//                        )
//                );
//        System.out.println(collect);
//    }

//Zadanie 14a
//Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia
// to samo sadanie tylko na Intiger a nie na Long j.w.

    private static void ex14a(List<Employee> employees) {                    //ctrl alt V tworzy zmienne i przypisuje do wartości//
        System.out.println(" ");
        System.out.println("ZADANIE 14A");

        Map<String, Integer> collect = employees.stream()
                .collect(                                                    //Steam<Emploeyy>
                        groupingBy(
                                Employee::getFirstName,
                                collectingAndThen(
                                        counting(),
                                        Long::intValue                      //konwersja z longa na inta
                                )                                           // j8 readme collecting and then,
                        )
                );
        System.out.println(collect);
    }

//Zadanie 14c
//c. Stwórz mapę gdzie kluczem będzie Imię a wartościa liczba jego wystąpienia, zbierz imioną tylko te które kończą się na "a"
// rozpisz sobie graficznie jak nie wiesz od czego zacząć!!!

    private static void ex14c(List<Employee> employees) {        //ctrl alt V tworzy zmienne i przypisuje do wartości//
        System.out.println(" ");
        System.out.println("ZADANIE 14C");

        Map<String, Long> a = employees.stream()
//                .sorted(Comparator.comparing(Employee::getFirstName)) // np dla posortowania
                .collect(
                        groupingBy(
                                Employee::getFirstName,
                                filtering(
                                        e -> e.getFirstName().endsWith("a"),
//                                        e -> !e.getFirstName().endsWith("a"),   można tu zrobić np. negację
                                        counting()
                                )
                        )
                );
        System.out.println(a);
    }

    //Zadanie 15
//Znajdz osobę najlepiej zarabiająca
    private static void ex15(List<Employee> employees) {        //ctrl alt V tworzy zmienne i przypisuje do wartości//
        System.out.println(" ");
        System.out.println("ZADANIE 15");

        //        Optional<Employee> collect = employees.stream()         // Optional - dodatkowe opakowanie(wrapper). Sa po to, żeby unikać NullPoitException
//                .collect(
//                        maxBy(
//                                Comparator.comparing(Employee::getSalary)
//                        )
//                );
        Optional<Employee> collect = employees.stream().max(Comparator.comparing(Employee::getSalary)); // to samo co wyzej tylko lepiej

        System.out.println(collect);
        // do sprawdzenia: IntStatistic, S
    }

    //Zadanie 16
//Znajdz profesję osoby najgorzej zarabiającej / najlepiej
    private static void ex16(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 16");

        Optional<Employee> collect = employees.stream().min(Comparator.comparing(Employee::getSalary));
        System.out.println(collect);

        Optional<Employee> collect2 = employees.stream().max(Comparator.comparing(Employee::getSalary));
        System.out.println(collect2);

        // do sprawdzenia: IntStatistic, S
    }

    //Zadanie 17
//Wylicz średnia zarobków kobiet
    private static void ex17(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 17");

        double femaleAvarageSalary = employees.stream()
                .filter(e -> e.getFirstName().endsWith("a"))
                .mapToDouble(Employee::getSalary).average().getAsDouble();

        System.out.println(femaleAvarageSalary);
    }

    //Zadanie 18
//Wylicz średnia zarobków mężczyzn
    private static void ex18(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 18");

        double maleAvarageSalary = employees.stream()
                .filter(e -> !e.getFirstName().endsWith("a"))
                .mapToDouble(Employee::getSalary).average().getAsDouble();

        System.out.println(maleAvarageSalary);
    }

    //Zadanie 19
//Pogrupuj osoby po nazwisku, tylko te którch imię zaczyna się na K
    private static void ex19(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 19");

        employees.stream()
                .filter(e -> e.getFirstName().startsWith("K"))
                .sorted(Comparator.comparing(Employee::getLastName))
                .forEach(System.out::println);

    }

    //Zadanie 20
//Stwórz Mapę której kluczem będzie profesja a kluczem lista wszystkich zarobkow mniejszych niz 2000 (użyj grupingBy, mapping, filtering)
    private static void ex20(List<Employee> employees) {
        System.out.println(" ");
        System.out.println("ZADANIE 20");

        Map<String, List<Double>> collect = employees.stream()
                .filter(e -> e.getSalary() < 2000)
                .collect(
                        groupingBy(
                                Employee::getProfession,
                                mapping(Employee::getSalary,
                                        toList())
                        )

                );
        System.out.println(collect);

    }

}



