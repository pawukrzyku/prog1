package PatientComparator;

import hostpital.Disease;
import hostpital.Patient;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    private static String KOWALSKI = "Kowalski";

    //[aptient1, patient 2 .... patient5]
    //poll()
    //patient1 w tym przypadku - normalna kolejka


    @Override
    public int compare(Patient o1, Patient o2) {        // -1 wazny poprzedni, 0 rowne, 1 prawy wazniejszy
        boolean isFirstKowalski = o1.getSurname().equals("Kowalski");
        boolean isSecondKowalski = o2.getSurname().equals("Kowalski");

        if (!isFirstKowalski && isSecondKowalski) {
            return 1;
        } else if (isFirstKowalski && !isSecondKowalski) {
            return -1;
        } else {
            boolean is01SthSerious = o1.getDisease().equals(Disease.STH_SERIOUS);
            boolean is02SthSerious = o2.getDisease().equals(Disease.STH_SERIOUS);
            if (!is01SthSerious && is02SthSerious) {
                return 1;
            } else if (is01SthSerious && !is02SthSerious){
                return -1;
            }else {
                Integer o1Factor = o1.getDisease().getInfectiousness() * o1.getHowAngry();
                Integer o2Factor = o2.getDisease().getInfectiousness() * o2.getHowAngry();
            }
        }

        return 0;   //Jeżeli przyjdzie więcej kowalskich to występuje tzw natural order.
    }
}
