package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day2 {

    public static void main(String[] args) throws IOException {

        Stream<String> inputValues =
                Files.lines(
                        Path.of("src/main/resources/adventofcode/day2input"));

        List<String> listOfModuleMass = inputValues.collect(Collectors.toList());


    }
}
