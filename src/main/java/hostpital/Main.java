package hostpital;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Przychodnia last moments, witamy w rejestracji: \n");    // \n oznacza nowa linie


        String option;
        HospitalQueueService hospitalQueueService
                = new HospitalQueueService();
        do {
            printMenu();
            option = scanner.nextLine();
            if ("a".equals(option)) {
                System.out.println("Rejstracja nowego pacjenta");
                Patient newPatient = handleNewPatient(scanner);
                hospitalQueueService.addPatient(newPatient);
            } else if ("b".equals(option)) {
                Patient handledPatient =
                        hospitalQueueService.handlePatient();
                printPatientInfo(handledPatient);
            } else if ("c".equals(option)) {
                System.out.println(hospitalQueueService.queueSize());
            } else if ("d".equals(option)) {

            }
        } while (!"q".equals(option));

    }

    private static void printMenu() {
        System.out.println(
                "a. Rejstracja nowego pacjenta \n" +
                        "b. Obsłuż pacjetna \n" +
                        "c. Ilość pacjetów w kolejce \n" +
                        "d. Następny pacjent \n" +
                        "q. Wyjscie "
        );
    }

    private static void printPatientInfo(Patient handledPatient) {
        System.out.println(
                new StringBuilder()
                        .append("Pacjent ")
                        .append(handledPatient.getName())
                        .append(" ")
                        .append(handledPatient.getSurname())
                        .append(" został przyjęty")
                        .toString()
        );
    }

    private static Patient handleNewPatient(Scanner scanner) {
        System.out.println("Imie: ");
        String name = scanner.nextLine();
        System.out.println("Nazwisko: ");
        String surname = scanner.nextLine();
        System.out.println("Złość: ");
        Integer howAngry = Integer.valueOf(scanner.nextInt());
        System.out.println("Choroba: ");
        scanner.nextLine();
        String diseaseStringVal2 = scanner.next();
        Disease disease = Disease.valueOf(diseaseStringVal2);

        Patient patient = new Patient();
        patient.setName(name);
        patient.setSurname(surname);
        patient.setHowAngry(howAngry);
        patient.setDisese(disease);
        System.out.println("Zarejestrowano pacjenta");
        return patient;
    }
}
