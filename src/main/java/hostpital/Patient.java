package hostpital;

public class Patient {

    // POJO - PLAIN OLD JAVA OBJECTS

    private String name;
    private String surname;
    private int howAngry;
    private Disease disease;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getHowAngry() {
        return howAngry;
    }

    public void setHowAngry(int howAngry) {
        this.howAngry = howAngry;
    }

    public Disease getDisease() {
        return disease;
    }

    public void setDisese(Disease disease) {
        this.disease = disease;
    }

}
