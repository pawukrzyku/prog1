package queue;

public class LinkedQueue<T> implements Queue<T> {

    //Dwa rodzaje linkedlist: Single linked list oraz duble linked list
    //                         []->[]->[]->[]  i      []-><-[]->-<[]-><-[]

    private Link<T> head;

    // W DOMU ZROBIC POLL SIZE I ZROBIC NA DUBLE LISTE

    @Override
    //sprawdzic czy mamy czoło kolejki
    //jesli nie to stawic na nim nowy link
    //jesli mamu czolo kolejki to w petli przebiegamy po nastepnych ogniwach az do pustego
    //czyli LINk -> daj next. nic nie zwraca
    //[LINk, LINk, LINk, ] <- offer 50
    //[LINk -> daj next, LINk -> daj next, LINk -> daj next, .... ]

    //DO DOMU - PRZEROBIC NA DUBLE LINKED LIST (INFO NA PREVIUS)

    public void offer(T element) {
        Link<T> newLink = new Link<>(element);
        if (head == null) {
            //jezeli nie mamy czola kolejki to je ustawiamy
            head = newLink;
        } else {
            //mamy czolo, przebiegamy w petli
            //jezeli jest nastepny element idziemy na sam koniec - do "ogona"
            Link<T> link = this.head;
            //iterujemy az dojdziemy do ogona
            while (link.getNext() != null) {
                link = link.getNext();
            }
            link.setNext(newLink);
        }
    }

    @Override
    public T poll() {
        //jezeli kolejka jest pusta
        if (head == null) {
            System.out.println("Empty queue");
            return null;
        }
        //jezeli jest to pobieramy wartosc head'a

        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

// KOLEJKA = LINKED LISTA

    @Override
    public T peek() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

}
