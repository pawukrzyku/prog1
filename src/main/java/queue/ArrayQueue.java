package queue;

import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T> {


    private Object[] elements;              // inicjalizacja w konstruktorze:

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    //[1] 2 3
    // kopia tablocy gdzie jej wielkosc bedzie zawsze size +1
    //[]
    //[] <- 1
    //[] -> copyOf(stara tablica, rozmiar starej +1)
    //[ , ] elements[elements.lenght -1] ->145 np.
    //[145]
    //[145] <- 200
    // Arrays.copyOf([145], [145, ])
    //[ , ] elements[elements.lenght -1] ->145 np.


    //implementacja metod:

    @Override
    public void offer(T element) {          // tworzymy koopie tablicy zeby zwiekszyc jej rozmiar
        elements = Arrays.copyOf(
                elements, elements.length + 1);  //dodajemy element na 1 miejscu kolejki
        elements[elements.length - 1] = element;
    }

    @Override
    //pobiera pierwszy element zwraca i usuwa z kolejki
    //[12,45,20] <- poll()
    //12 - dostaje 1 element
    //[null, 45,20] - > copyOfRange

    public T poll() {       //pobiera pierwszy element z kolejki
        if (checkIfArrayIsEmpty()) return null;

        T element = (T) elements[0];
        elements = Arrays.copyOfRange(elements, 1, elements.length);
        return element;
    }

    //SOLID
    //DRY - dont repeat yourself - czysty kod
    @Override
    public T peek() {
        if (checkIfArrayIsEmpty()) return null;

        return (T) elements[0];
    }

    @Override
    public int size() {

        return elements.length;
    }

    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }

        return false;
    }

}
