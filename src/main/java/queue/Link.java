package queue;

//Generyki maja parapetr <T> - wartość generyczna
public class Link<T> {

    private T value;
    private Link<T> next;

    public Link(T value) {
        this.value = value;
    }

    public Link<T> getNext() {
        return next;
    }

    public Link<T> setNext(Link<T> next) {
        this.next = next;
        return this;
    }

    public T getValue() {
        return null;
    }
    //    private Link<T> previous;


}
