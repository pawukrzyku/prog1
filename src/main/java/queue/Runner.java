package queue;

public class Runner {

    public static void main(String[] args) {
        ArrayQueue<Integer>
                queue = new ArrayQueue<>();

        queue.offer(2);
        queue.offer(5);
        queue.offer(10);
        queue.offer(11);
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.size());

    }
}
