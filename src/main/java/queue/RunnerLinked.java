package queue;

public class RunnerLinked {
    public static void main(String[] args) {


        LinkedQueue<Integer> queue = new LinkedQueue<>();

        queue.offer(2);
        queue.offer(5);
        queue.offer(10);
        queue.offer(11);
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.size());
    }
}