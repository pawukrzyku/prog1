//wazne w programowaniu!

package queue;

//interfejs typ generyczny


public interface Queue<T> {

    void offer(T element);

    T poll();

    T peek();

    int size();
}
